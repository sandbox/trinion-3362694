<?php

/**
 * Implements hook_install().
 */
function trinion_zadachnik_install($is_syncing) {
  $config = \Drupal::service('config.factory')->getEditable('trinion_zadachnik.settings');
  $tz_statusy_zadach_vid = 'tz_statusy_zadach';
  $tz_prioritet_zadachi_vid = 'prioritet_zadachi';
  $tz_deatelnost_zadacha_vid = 'deatelnost_zadacha';
  $tz_project_vid = 'tz_proekt';

  $terms_data = [
    $tz_project_vid => [
      'project_nerazobrannaya_tid' => [
        'name' => 'Undecipherable',
      ],
    ],
    $tz_statusy_zadach_vid => [
      'status_novaya_tid' => [
        'name' => 'New',
        'field_tz_ikonka' => file_get_contents(__DIR__ . '/images/icons/status_novaya_tid.svg'),
      ],
      'status_v_rabote_tid' => [
        'name' => 'In progress',
        'field_tz_ikonka' => file_get_contents(__DIR__ . '/images/icons/status_v_rabote_tid.svg'),
      ],
      'status_vipolnenae_tid' => [
        'name' => 'Completed',
        'field_tz_ikonka' => file_get_contents(__DIR__ . '/images/icons/status_vipolnenae_tid.svg'),
      ],
      'status_zakrita_tid' => [
        'name' => 'Closed',
        'field_tz_ikonka' => file_get_contents(__DIR__ . '/images/icons/status_zakrita_tid.svg'),
      ],
      'status_na_dorabotku_tid' => [
        'name' => 'For revision',
        'field_tz_ikonka' => file_get_contents(__DIR__ . '/images/icons/status_na_dorabotku_tid.svg'),
      ],
      'status_otlozheno_tid' => [
        'name' => 'Postponed',
        'field_tz_ikonka' => file_get_contents(__DIR__ . '/images/icons/status_otlozheno_tid.svg'),
      ],
      'status_nuzhen_otklik_tid' => [
        'name' => 'Need feedback',
        'field_tz_ikonka' => file_get_contents(__DIR__ . '/images/icons/status_nuzhen_otklik_tid.svg'),
      ],
      'status_otmenena_tid' => [
        'name' => 'Cancelled',
        'field_tz_ikonka' => file_get_contents(__DIR__ . '/images/icons/status_otmenena_tid.svg'),
      ],
    ],
    $tz_prioritet_zadachi_vid => [
      'prioritet_nizkiy' => [
        'name' => 'Low',
        'field_tz_ikonka' => file_get_contents(__DIR__ . '/images/icons/prioritet_nizkiy.svg'),
      ],
      'prioritet_normalniy' => [
        'name' => 'Normal',
        'field_tz_ikonka' => file_get_contents(__DIR__ . '/images/icons/prioritet_normalniy.svg'),
      ],
      'prioritet_visokiy' => [
        'name' => 'Hight',
        'field_tz_ikonka' => file_get_contents(__DIR__ . '/images/icons/prioritet_visokiy.svg'),
      ],
      'prioritet_srochniy' => [
        'name' => 'Emergency',
        'field_tz_ikonka' => file_get_contents(__DIR__ . '/images/icons/prioritet_srochniy.svg'),
      ],
      'prioritet_nemedlenniy' => [
        'name' => 'Immediate',
        'field_tz_ikonka' => file_get_contents(__DIR__ . '/images/icons/prioritet_nemedlenniy.svg'),
      ],
    ],
    $tz_deatelnost_zadacha_vid => [
      'deatelnost_oshibka' => [
        'name' => 'Error',
      ],
      'deatelnost_uluchshenie' => [
        'name' => 'Improvement',
      ],
      'deatelnost_podderzhka' => [
        'name' => 'Support',
      ],
    ],
  ];
  foreach ($terms_data as $vid => $terms) {
    foreach ($terms as $conf_name => $term_data) {
      $term_data['vid'] = $vid;
      $term = \Drupal\taxonomy\Entity\Term::create($term_data);
      $term->save();
      $config->set($conf_name, $term->id());
      if ($conf_name == 'status_novaya_tid')
        $status_novaya_uuid = $term->uuid();
    }
  }

  $config->save();

  $config = \Drupal::service('config.factory')->getEditable('field.field.node.zadacha.field_tz_status_zadachi');
  $data = $config->getRawData();
  $data['default_value'][0]['target_uuid'] = $status_novaya_uuid;
  $config->setData($data);
  $config->save();
  drupal_flush_all_caches();

  $role_object = \Drupal\user\Entity\Role::load('t_zadachnik');
  $role_object->grantPermission('create zadacha content');
  $role_object->grantPermission('edit own zadacha content');
  $role_object->grantPermission('use views bulk edit');
  $role_object->save();

  // Enable Tasks loging
  $config_change_log = \Drupal::service('config.factory')->getEditable('trinion_change_log.settings');
  $bundles = $config_change_log->get('bundles');
  if (!$bundles)
    $bundles = [];
  if (!in_array('zadacha', $bundles)) {
    $bundles[] = 'zadacha';
    $config_change_log->set('bundles', $bundles);
    $config_change_log->save();
  }

  $config = \Drupal::configFactory()->getEditable('core.entity_form_display.user.user.default');
  if (!$config->isNew()) {
    $config->set('content.field_tz_sotrudnik', [
      'type' => 'boolean_checkbox',
      'weight' => '10',
      'region' => 'content',
      'settings' => ['display_label' => TRUE],
      'third_party_settings' => [],
    ]);
    $config->clear('hidden.field_tz_sotrudnik');
  }
}

function trinion_zadachnik_update_8001(&$sandbox) {
  $to_delete = [
  ];
  foreach ($to_delete as $conf) {
    \Drupal::configFactory()->getEditable($conf)->delete();
  }

  $prefixes = [
    'core.entity_view_display.comment.tz_k_zadache.default',
  ];

  if (!isset($sandbox['total'])) {
    $sandbox['total'] = count($prefixes);
    $sandbox['current'] = 0;
  }
  $results = [];
  foreach ($prefixes as $key => $prefix) {
    if ($key == $sandbox['current']) {
      $results[$prefix] = trinion_base_update_or_install_config($prefix, __DIR__);
      $msg = $prefix;
    }
  }
  $sandbox['current']++;
  $sandbox['#finished'] = $sandbox['current'] == count($prefixes);
  return $msg;
}

function trinion_zadachnik_update_8002(&$sandbox) {
  $to_delete = [
  ];
  foreach ($to_delete as $conf) {
    \Drupal::configFactory()->getEditable($conf)->delete();
  }

  $prefixes = [
    'field.storage.user.field_tz_proekt',
    'field.field.user.user.field_tz_proekt',
    'core.entity_view_display.user.user.default',
    'core.entity_form_display.user.user.default',
    'views.view.zadachi_naznachennye_mne',
  ];

  if (!isset($sandbox['total'])) {
    $sandbox['total'] = count($prefixes);
    $sandbox['current'] = 0;
  }
  $results = [];
  foreach ($prefixes as $key => $prefix) {
    if ($key == $sandbox['current']) {
      $results[$prefix] = trinion_base_update_or_install_config($prefix, __DIR__);
      $msg = $prefix;
    }
  }
  $sandbox['current']++;
  $sandbox['#finished'] = $sandbox['current'] == count($prefixes);
  return $msg;
}

function trinion_zadachnik_update_8003(&$sandbox) {
  $to_delete = [
  ];
  foreach ($to_delete as $conf) {
    \Drupal::configFactory()->getEditable($conf)->delete();
  }

  $prefixes = [
    'taxonomy.vocabulary.tz_statusy_zadach',
    'taxonomy.vocabulary.tz_proekt',
    'taxonomy.vocabulary.prioritet_zadachi',
    'taxonomy.vocabulary.kategoriya_zadachi',
    'taxonomy.vocabulary.deatelnost_zadacha',
    'node.type.zadacha',
    'language.content_settings.taxonomy_term.tz_statusy_zadach',
    'field.field.node.zadacha.field_tz_vrem_vypoleniya_zadachi',
    'field.field.node.zadacha.field_tz_vremya_nachala_zadachi',
    'field.field.node.zadacha.field_tz_tema',
    'field.field.node.zadacha.field_tz_status_zadachi',
    'field.field.node.zadacha.field_tz_ssylka_na_obekt',
    'field.field.node.zadacha.field_tz_proekt',
    'field.field.node.zadacha.field_tz_prioritet',
    'field.field.node.zadacha.field_tz_opisanie',
    'field.field.node.zadacha.field_tz_kategoriya_zadachi',
    'field.field.node.zadacha.field_tz_deyatelnost',
    'field.field.node.zadacha.field_tz_comments',
    'core.base_field_override.node.zadacha.title',
    'field.field.node.zadacha.field_tz_role',
    'field.field.node.zadacha.field_tz_otvetstvennyy',
    'views.view.zadachi_naznachennye_mne',
  ];

  if (!isset($sandbox['total'])) {
    $sandbox['total'] = count($prefixes);
    $sandbox['current'] = 0;
  }
  $results = [];
  foreach ($prefixes as $key => $prefix) {
    if ($key == $sandbox['current']) {
      $results[$prefix] = trinion_base_update_or_install_config($prefix, __DIR__);
      $msg = $prefix;
    }
  }
  $sandbox['current']++;
  $sandbox['#finished'] = $sandbox['current'] == count($prefixes);
  return $msg;
}

function trinion_zadachnik_update_8004(&$sandbox) {
  $to_delete = [
  ];
  foreach ($to_delete as $conf) {
    \Drupal::configFactory()->getEditable($conf)->delete();
  }

  $prefixes = [
    'views.view.zadachi_naznachennye_mne',
  ];

  if (!isset($sandbox['total'])) {
    $sandbox['total'] = count($prefixes);
    $sandbox['current'] = 0;
  }
  $results = [];
  foreach ($prefixes as $key => $prefix) {
    if ($key == $sandbox['current']) {
      $results[$prefix] = trinion_base_update_or_install_config($prefix, __DIR__);
      $msg = $prefix;
    }
  }
  $sandbox['current']++;
  $sandbox['#finished'] = $sandbox['current'] == count($prefixes);
  return $msg;
}

function trinion_zadachnik_update_8005(&$sandbox) {
  $to_delete = [
  ];
  foreach ($to_delete as $conf) {
    \Drupal::configFactory()->getEditable($conf)->delete();
  }

  $prefixes = [
    'core.entity_form_display.node.zadacha.default',
    'views.view.zadachi_naznachennye_mne',
  ];

  if (!isset($sandbox['total'])) {
    $sandbox['total'] = count($prefixes);
    $sandbox['current'] = 0;
  }
  $results = [];
  foreach ($prefixes as $key => $prefix) {
    if ($key == $sandbox['current']) {
      $results[$prefix] = trinion_base_update_or_install_config($prefix, __DIR__);
      $msg = $prefix;
    }
  }
  $sandbox['current']++;
  $sandbox['#finished'] = $sandbox['current'] == count($prefixes);
  return $msg;
}

function trinion_zadachnik_update_8006(&$sandbox) {
  $to_delete = [
  ];
  foreach ($to_delete as $conf) {
    \Drupal::configFactory()->getEditable($conf)->delete();
  }

  $prefixes = [
    'core.entity_form_display.node.zadacha.default',
    'views.view.zadachi_naznachennye_mne',
  ];

  if (!isset($sandbox['total'])) {
    $sandbox['total'] = count($prefixes);
    $sandbox['current'] = 0;
  }
  $results = [];
  foreach ($prefixes as $key => $prefix) {
    if ($key == $sandbox['current']) {
      $results[$prefix] = trinion_base_update_or_install_config($prefix, __DIR__);
      $msg = $prefix;
    }
  }
  $sandbox['current']++;
  $sandbox['#finished'] = $sandbox['current'] == count($prefixes);
  return $msg;
}

function trinion_zadachnik_update_8007(&$sandbox) {
  $to_delete = [
  ];
  foreach ($to_delete as $conf) {
    \Drupal::configFactory()->getEditable($conf)->delete();
  }

  $prefixes = [
    'views.view.ssylka_na_suschnost_proekty_v_zadache',
    'field.field.node.zadacha.field_tz_proekt',
  ];

  if (!isset($sandbox['total'])) {
    $sandbox['total'] = count($prefixes);
    $sandbox['current'] = 0;
  }
  $results = [];
  foreach ($prefixes as $key => $prefix) {
    if ($key == $sandbox['current']) {
      $results[$prefix] = trinion_base_update_or_install_config($prefix, __DIR__);
      $msg = $prefix;
    }
  }
  $sandbox['current']++;
  $sandbox['#finished'] = $sandbox['current'] == count($prefixes);
  return $msg;
}

function trinion_zadachnik_update_8008(&$sandbox) {
  $config = \Drupal::service('config.factory')->getEditable('trinion_zadachnik.settings');
  $tz_statusy_zadach_vid = 'tz_statusy_zadach';

  $terms_data = [
    $tz_statusy_zadach_vid => [
      'status_zakrita_tid' => [
        'name' => 'Closed',
      ],
    ],
  ];
  foreach ($terms_data as $vid => $terms) {
    foreach ($terms as $conf_name => $term_data) {
      $term_data['vid'] = $vid;
      $term = \Drupal\taxonomy\Entity\Term::create($term_data);
      $term->save();
      $config->set($conf_name, $term->id());
    }
  }
  $msg = 'Add term';

  $config->save();

  $to_delete = [
  ];
  foreach ($to_delete as $conf) {
    \Drupal::configFactory()->getEditable($conf)->delete();
  }

  $prefixes = [
  ];

  if (!isset($sandbox['total'])) {
    $sandbox['total'] = count($prefixes);
    $sandbox['current'] = 0;
  }
  $results = [];
  foreach ($prefixes as $key => $prefix) {
    if ($key == $sandbox['current']) {
      $results[$prefix] = trinion_base_update_or_install_config($prefix, __DIR__);
      $msg = $prefix;
    }
  }

  $sandbox['#finished'] = TRUE;
  return $msg;
}

function trinion_zadachnik_update_8009(&$sandbox) {
  $sandbox['#finished'] = TRUE;
  return 'ok';
}

function trinion_zadachnik_update_8010(&$sandbox) {
  $to_delete = [
  ];
  foreach ($to_delete as $conf) {
    \Drupal::configFactory()->getEditable($conf)->delete();
  }

  $prefixes = [
    'field.storage.user.field_tz_redaktirovat_zadachi',
    'field.field.user.user.field_tz_redaktirovat_zadachi',
  ];

  if (!isset($sandbox['total'])) {
    $sandbox['total'] = count($prefixes);
    $sandbox['current'] = 0;
  }
  $results = [];
  foreach ($prefixes as $key => $prefix) {
    if ($key == $sandbox['current']) {
      $results[$prefix] = trinion_base_update_or_install_config($prefix, __DIR__);
      $msg = $prefix;
    }
  }
  $sandbox['current']++;
  $sandbox['#finished'] = $sandbox['current'] == count($prefixes);
  return $msg;
}

function trinion_zadachnik_update_8011(&$sandbox) {
  $config = \Drupal::service('config.factory')->getEditable('trinion_zadachnik.settings');
  $tz_project_vid = 'tz_proekt';

  $terms_data = [
    $tz_project_vid => [
      'project_nerazobrannaya_tid' => [
        'name' => 'Undecipherable',
      ],
    ],
  ];
  foreach ($terms_data as $vid => $terms) {
    foreach ($terms as $conf_name => $term_data) {
      $term_data['vid'] = $vid;
      $term = \Drupal\taxonomy\Entity\Term::create($term_data);
      $term->save();
      $config->set($conf_name, $term->id());
    }
  }

  $config->save();
  $sandbox['#finished'] = TRUE;
  return '';
}

function trinion_zadachnik_update_8012(&$sandbox) {
  $to_delete = [
  ];
  foreach ($to_delete as $conf) {
    \Drupal::configFactory()->getEditable($conf)->delete();
  }

  $prefixes = [
    'views.view.zadachi_naznachennye_mne',
  ];

  if (!isset($sandbox['total'])) {
    $sandbox['total'] = count($prefixes);
    $sandbox['current'] = 0;
  }
  $results = [];
  foreach ($prefixes as $key => $prefix) {
    if ($key == $sandbox['current']) {
      $results[$prefix] = trinion_base_update_or_install_config($prefix, __DIR__);
      $msg = $prefix;
    }
  }
  $sandbox['current']++;
  $sandbox['#finished'] = $sandbox['current'] == count($prefixes);
  return $msg;
}

function trinion_zadachnik_update_8013(&$sandbox) {
  $to_delete = [
  ];
  foreach ($to_delete as $conf) {
    \Drupal::configFactory()->getEditable($conf)->delete();
  }

  $prefixes = [
    'core.entity_form_mode.node.bulk_edit',
    'core.entity_form_display.node.zadacha.bulk_edit',
    'views.view.zadachi_naznachennye_mne',
  ];

  if (!isset($sandbox['total'])) {
    $sandbox['total'] = count($prefixes);
    $sandbox['current'] = 0;
  }
  $results = [];
  foreach ($prefixes as $key => $prefix) {
    if ($key == $sandbox['current']) {
      $results[$prefix] = trinion_base_update_or_install_config($prefix, __DIR__);
      $msg = $prefix;
    }
  }
  $sandbox['current']++;
  $sandbox['#finished'] = $sandbox['current'] == count($prefixes);
  return $msg;
}

function trinion_zadachnik_update_8014(&$sandbox) {
  $sandbox['#finished'] = TRUE;
  return 'ok';
}

function trinion_zadachnik_update_8015(&$sandbox) {
  $to_delete = [
  ];
  foreach ($to_delete as $conf) {
    \Drupal::configFactory()->getEditable($conf)->delete();
  }

  $prefixes = [
    'views.view.zadachi_naznachennye_mne',
  ];

  if (!isset($sandbox['total'])) {
    $sandbox['total'] = count($prefixes);
    $sandbox['current'] = 0;
  }
  $results = [];
  foreach ($prefixes as $key => $prefix) {
    if ($key == $sandbox['current']) {
      $results[$prefix] = trinion_base_update_or_install_config($prefix, __DIR__);
      $msg = $prefix;
    }
  }
  $sandbox['current']++;
  $sandbox['#finished'] = $sandbox['current'] == count($prefixes);
  return $msg;
}

function trinion_zadachnik_update_8016(&$sandbox) {
  $to_delete = [
  ];
  foreach ($to_delete as $conf) {
    \Drupal::configFactory()->getEditable($conf)->delete();
  }

  $prefixes = [
    'views.view.zadachi_naznachennye_mne',
  ];

  if (!isset($sandbox['total'])) {
    $sandbox['total'] = count($prefixes);
    $sandbox['current'] = 0;
  }
  $results = [];
  foreach ($prefixes as $key => $prefix) {
    if ($key == $sandbox['current']) {
      $results[$prefix] = trinion_base_update_or_install_config($prefix, __DIR__);
      $msg = $prefix;
    }
  }
  $sandbox['current']++;
  $sandbox['#finished'] = $sandbox['current'] == count($prefixes);
  return $msg;
}

function trinion_zadachnik_update_8017(&$sandbox) {
  $to_delete = [
  ];
  foreach ($to_delete as $conf) {
    \Drupal::configFactory()->getEditable($conf)->delete();
  }

  $prefixes = [
    'views.view.zadachi_naznachennye_mne',
  ];

  if (!isset($sandbox['total'])) {
    $sandbox['total'] = count($prefixes);
    $sandbox['current'] = 0;
  }
  $results = [];
  foreach ($prefixes as $key => $prefix) {
    if ($key == $sandbox['current']) {
      $results[$prefix] = trinion_base_update_or_install_config($prefix, __DIR__);
      $msg = $prefix;
    }
  }
  $sandbox['current']++;
  $sandbox['#finished'] = $sandbox['current'] == count($prefixes);
  return $msg;
}

function trinion_zadachnik_update_8018(&$sandbox) {
  $to_delete = [
  ];
  foreach ($to_delete as $conf) {
    \Drupal::configFactory()->getEditable($conf)->delete();
  }

  $prefixes = [
    'views.view.zadachi_naznachennye_mne',
  ];

  if (!isset($sandbox['total'])) {
    $sandbox['total'] = count($prefixes);
    $sandbox['current'] = 0;
  }
  $results = [];
  foreach ($prefixes as $key => $prefix) {
    if ($key == $sandbox['current']) {
      $results[$prefix] = trinion_base_update_or_install_config($prefix, __DIR__);
      $msg = $prefix;
    }
  }
  $sandbox['current']++;
  $sandbox['#finished'] = $sandbox['current'] == count($prefixes);
  return $msg;
}

function trinion_zadachnik_update_8019(&$sandbox) {
  $to_delete = [
  ];
  foreach ($to_delete as $conf) {
    \Drupal::configFactory()->getEditable($conf)->delete();
  }

  $prefixes = [
    'views.view.zadachi_naznachennye_mne',
  ];

  if (!isset($sandbox['total'])) {
    $sandbox['total'] = count($prefixes);
    $sandbox['current'] = 0;
  }
  $results = [];
  foreach ($prefixes as $key => $prefix) {
    if ($key == $sandbox['current']) {
      $results[$prefix] = trinion_base_update_or_install_config($prefix, __DIR__);
      $msg = $prefix;
    }
  }

  $role_object = \Drupal\user\Entity\Role::load('authenticated');
  $role_object->grantPermission('use views bulk edit');
  $role_object->save();

  $sandbox['current']++;
  $sandbox['#finished'] = $sandbox['current'] == count($prefixes);
  return $msg;
}

function trinion_zadachnik_update_8020(&$sandbox) {
  $to_delete = [
  ];
  foreach ($to_delete as $conf) {
    \Drupal::configFactory()->getEditable($conf)->delete();
  }

  $prefixes = [
    'field.storage.taxonomy_term.field_tz_icon',
    'field.field.taxonomy_term.tz_statusy_zadach.field_tz_icon',
    'core.entity_view_display.taxonomy_term.tz_statusy_zadach.default',
    'core.entity_form_display.taxonomy_term.tz_statusy_zadach.default',
  ];

  if (!isset($sandbox['total'])) {
    $sandbox['total'] = count($prefixes);
    $sandbox['current'] = 0;
  }
  $results = [];
  foreach ($prefixes as $key => $prefix) {
    if ($key == $sandbox['current']) {
      $results[$prefix] = trinion_base_update_or_install_config($prefix, __DIR__);
      $msg = $prefix;
    }
  }

  $role_object = \Drupal\user\Entity\Role::load('authenticated');
  $role_object->grantPermission('use views bulk edit');
  $role_object->save();

  $sandbox['current']++;
  $sandbox['#finished'] = $sandbox['current'] == count($prefixes);
  return $msg;
}

function trinion_zadachnik_update_8021(&$sandbox) {
  $to_delete = [
  ];
  foreach ($to_delete as $conf) {
    \Drupal::configFactory()->getEditable($conf)->delete();
  }

  $prefixes = [
    'field.field.taxonomy_term.tz_statusy_zadach.field_tz_icon',
  ];

  if (!isset($sandbox['total'])) {
    $sandbox['total'] = count($prefixes);
    $sandbox['current'] = 0;
  }
  $results = [];
  foreach ($prefixes as $key => $prefix) {
    if ($key == $sandbox['current']) {
      $results[$prefix] = trinion_base_update_or_install_config($prefix, __DIR__);
      $msg = $prefix;
    }
  }

  $role_object = \Drupal\user\Entity\Role::load('authenticated');
  $role_object->grantPermission('use views bulk edit');
  $role_object->save();

  $sandbox['current']++;
  $sandbox['#finished'] = $sandbox['current'] == count($prefixes);
  return $msg;
}

function trinion_zadachnik_update_8022(&$sandbox) {
  $to_delete = [
  ];
  foreach ($to_delete as $conf) {
    \Drupal::configFactory()->getEditable($conf)->delete();
  }

  $prefixes = [
    'field.field.taxonomy_term.tz_statusy_zadach.field_tz_icon',
  ];

  if (!isset($sandbox['total'])) {
    $sandbox['total'] = count($prefixes);
    $sandbox['current'] = 0;
  }
  $results = [];
  foreach ($prefixes as $key => $prefix) {
    if ($key == $sandbox['current']) {
      $results[$prefix] = trinion_base_update_or_install_config($prefix, __DIR__);
      $msg = $prefix;
    }
  }

  $role_object = \Drupal\user\Entity\Role::load('authenticated');
  $role_object->grantPermission('use views bulk edit');
  $role_object->save();

  $sandbox['current']++;
  $sandbox['#finished'] = $sandbox['current'] == count($prefixes);
  return $msg;
}

function trinion_zadachnik_update_8023(&$sandbox) {
  $to_delete = [
  ];
  foreach ($to_delete as $conf) {
    \Drupal::configFactory()->getEditable($conf)->delete();
  }

  $prefixes = [
    'field.storage.node.field_tz_file',
    'field.field.node.zadacha.field_tz_file',
    'core.entity_form_display.node.zadacha.default',
    'core.entity_form_display.node.zadacha.bulk_edit',
    'core.entity_view_display.node.zadacha.teaser',
    'core.entity_view_display.node.zadacha.default',
  ];

  if (!isset($sandbox['total'])) {
    $sandbox['total'] = count($prefixes);
    $sandbox['current'] = 0;
  }
  $results = [];
  foreach ($prefixes as $key => $prefix) {
    if ($key == $sandbox['current']) {
      $results[$prefix] = trinion_base_update_or_install_config($prefix, __DIR__);
      $msg = $prefix;
    }
  }

  $role_object = \Drupal\user\Entity\Role::load('authenticated');
  $role_object->grantPermission('use views bulk edit');
  $role_object->save();

  $sandbox['current']++;
  $sandbox['#finished'] = $sandbox['current'] == count($prefixes);
  return $msg;
}

function trinion_zadachnik_update_8024(&$sandbox) {
  $to_delete = [
  ];
  foreach ($to_delete as $conf) {
    \Drupal::configFactory()->getEditable($conf)->delete();
  }

  $prefixes = [
    'views.view.zadachi_naznachennye_mne',
  ];

  if (!isset($sandbox['total'])) {
    $sandbox['total'] = count($prefixes);
    $sandbox['current'] = 0;
  }
  $results = [];
  foreach ($prefixes as $key => $prefix) {
    if ($key == $sandbox['current']) {
      $results[$prefix] = trinion_base_update_or_install_config($prefix, __DIR__);
      $msg = $prefix;
    }
  }

  $role_object = \Drupal\user\Entity\Role::load('authenticated');
  $role_object->grantPermission('use views bulk edit');
  $role_object->save();

  $sandbox['current']++;
  $sandbox['#finished'] = $sandbox['current'] == count($prefixes);
  return $msg;
}

function trinion_zadachnik_update_8025(&$sandbox) {
  $to_delete = [
  ];
  foreach ($to_delete as $conf) {
    \Drupal::configFactory()->getEditable($conf)->delete();
  }

  $prefixes = [
    'field.storage.node.field_tz_related_tasks',
    'field.field.node.zadacha.field_tz_related_tasks',
    'core.entity_form_display.node.zadacha.default',
    'core.entity_form_display.node.zadacha.bulk_edit',
    'core.entity_view_display.node.zadacha.teaser',
    'core.entity_view_display.node.zadacha.default',
  ];

  if (!isset($sandbox['total'])) {
    $sandbox['total'] = count($prefixes);
    $sandbox['current'] = 0;
  }
  $results = [];
  foreach ($prefixes as $key => $prefix) {
    if ($key == $sandbox['current']) {
      $results[$prefix] = trinion_base_update_or_install_config($prefix, __DIR__);
      $msg = $prefix;
    }
  }

  $role_object = \Drupal\user\Entity\Role::load('authenticated');
  $role_object->grantPermission('use views bulk edit');
  $role_object->save();

  $sandbox['current']++;
  $sandbox['#finished'] = $sandbox['current'] == count($prefixes);
  return $msg;
}

function trinion_zadachnik_update_8026(&$sandbox) {
  $to_delete = [
  ];
  foreach ($to_delete as $conf) {
    \Drupal::configFactory()->getEditable($conf)->delete();
  }

  $prefixes = [
    'field.storage.comment.field_tz_files',
    'field.field.comment.tz_k_zadache.field_tz_files',
    'core.entity_view_display.comment.tz_k_zadache.default',
    'core.entity_form_display.comment.tz_k_zadache.default',
  ];

  if (!isset($sandbox['total'])) {
    $sandbox['total'] = count($prefixes);
    $sandbox['current'] = 0;
  }
  $results = [];
  foreach ($prefixes as $key => $prefix) {
    if ($key == $sandbox['current']) {
      $results[$prefix] = trinion_base_update_or_install_config($prefix, __DIR__);
      $msg = $prefix;
    }
  }

  $role_object = \Drupal\user\Entity\Role::load('authenticated');
  $role_object->grantPermission('use views bulk edit');
  $role_object->save();

  $sandbox['current']++;
  $sandbox['#finished'] = $sandbox['current'] == count($prefixes);
  return $msg;
}

function trinion_zadachnik_update_8027(&$sandbox) {
  $prefixes = [
    'field.storage.taxonomy_term.field_tz_ikonka',
    'field.field.taxonomy_term.tz_statusy_zadach.field_tz_ikonka',
    'field.field.taxonomy_term.prioritet_zadachi.field_tz_ikonka',
    'core.entity_view_display.taxonomy_term.prioritet_zadachi.default',
    'core.entity_form_display.taxonomy_term.prioritet_zadachi.default',
    'core.entity_view_display.taxonomy_term.tz_statusy_zadach.default',
    'core.entity_form_display.taxonomy_term.tz_statusy_zadach.default',
    'core.entity_view_display.node.zadacha.default',
    'views.view.zadachi_naznachennye_mne',
  ];
  $msg = trinion_base_common_update_operations($sandbox, $prefixes, __DIR__);

  $config = \Drupal::service('config.factory')->getEditable('trinion_zadachnik.settings');
  $tz_statusy_zadach_vid = 'tz_statusy_zadach';
  $tz_prioritet_zadachi_vid = 'prioritet_zadachi';

  $terms_data = [
    $tz_statusy_zadach_vid => [
      'status_novaya_tid' => [
        'name' => 'New',
        'field_tz_ikonka' => file_get_contents(__DIR__ . '/images/icons/status_novaya_tid.svg'),
      ],
      'status_v_rabote_tid' => [
        'name' => 'In progress',
        'field_tz_ikonka' => file_get_contents(__DIR__ . '/images/icons/status_v_rabote_tid.svg'),
      ],
      'status_vipolnenae_tid' => [
        'name' => 'Completed',
        'field_tz_ikonka' => file_get_contents(__DIR__ . '/images/icons/status_vipolnenae_tid.svg'),
      ],
      'status_zakrita_tid' => [
        'name' => 'Closed',
        'field_tz_ikonka' => file_get_contents(__DIR__ . '/images/icons/status_zakrita_tid.svg'),
      ],
      'status_na_dorabotku_tid' => [
        'name' => 'For revision',
        'field_tz_ikonka' => file_get_contents(__DIR__ . '/images/icons/status_na_dorabotku_tid.svg'),
      ],
      'status_otlozheno_tid' => [
        'name' => 'Postponed',
        'field_tz_ikonka' => file_get_contents(__DIR__ . '/images/icons/status_otlozheno_tid.svg'),
      ],
      'status_nuzhen_otklik_tid' => [
        'name' => 'Need feedback',
        'field_tz_ikonka' => file_get_contents(__DIR__ . '/images/icons/status_nuzhen_otklik_tid.svg'),
      ],
      'status_otmenena_tid' => [
        'name' => 'Cancelled',
        'field_tz_ikonka' => file_get_contents(__DIR__ . '/images/icons/status_otmenena_tid.svg'),
      ],
    ],
    $tz_prioritet_zadachi_vid => [
      'prioritet_nizkiy' => [
        'name' => 'Low',
        'field_tz_ikonka' => file_get_contents(__DIR__ . '/images/icons/prioritet_nizkiy.svg'),
      ],
      'prioritet_normalniy' => [
        'name' => 'Normal',
        'field_tz_ikonka' => file_get_contents(__DIR__ . '/images/icons/prioritet_normalniy.svg'),
      ],
      'prioritet_visokiy' => [
        'name' => 'Hight',
        'field_tz_ikonka' => file_get_contents(__DIR__ . '/images/icons/prioritet_visokiy.svg'),
      ],
      'prioritet_srochniy' => [
        'name' => 'Emergency',
        'field_tz_ikonka' => file_get_contents(__DIR__ . '/images/icons/prioritet_srochniy.svg'),
      ],
      'prioritet_nemedlenniy' => [
        'name' => 'Immediate',
        'field_tz_ikonka' => file_get_contents(__DIR__ . '/images/icons/prioritet_nemedlenniy.svg'),
      ],
    ],
  ];

  foreach ($terms_data as $vid => $terms) {
    foreach ($terms as $conf_name => $term_data) {
      if (($tid = $config->get($conf_name)) && ($term = \Drupal\taxonomy\Entity\Term::load($tid))) {
        $term->field_tz_ikonka = $term_data['field_tz_ikonka'];
      }
      else {
        $term_data['vid'] = $vid;
        $term = \Drupal\taxonomy\Entity\Term::create($term_data);
      }
      $term->save();
      $config->set($conf_name, $term->id());
    }
  }
  $config->save();
  return $msg;
}


function trinion_zadachnik_update_8028(&$sandbox) {
  $prefixes = [
    'field.field.taxonomy_term.tz_statusy_zadach.field_tz_ikonka',
    'field.field.taxonomy_term.prioritet_zadachi.field_tz_ikonka',
    'core.entity_view_display.taxonomy_term.tz_statusy_zadach.default',
    'core.entity_form_display.taxonomy_term.tz_statusy_zadach.default',
    'views.view.zadachi_naznachennye_mne',
  ];
  $msg = trinion_base_common_update_operations($sandbox, $prefixes, __DIR__);
  return $msg;
}

function trinion_zadachnik_update_8029(&$sandbox) {
  $config_change_log = \Drupal::service('config.factory')->getEditable('trinion_change_log.settings');
  $bundles = $config_change_log->get('bundles');
  if (!in_array('zadacha', $bundles)) {
    $bundles[] = 'zadacha';
    $config_change_log->set('bundles', $bundles);
    $config_change_log->save();
  }

  $prefixes = [
    'core.entity_view_display.comment.tz_k_zadache.default',
  ];
  $msg = trinion_base_common_update_operations($sandbox, $prefixes, __DIR__);
  return $msg;
}

function trinion_zadachnik_update_8030(&$sandbox) {
  $prefixes = [
    'core.entity_view_display.node.zadacha.default',
  ];
  $msg = trinion_base_common_update_operations($sandbox, $prefixes, __DIR__);
  return $msg;
}

function trinion_zadachnik_update_8031(&$sandbox) {
  $prefixes = [
    'core.entity_form_display.node.zadacha.default',
    'core.entity_view_display.node.zadacha.default',
    'views.view.zadachi_naznachennye_mne',
  ];
  $msg = trinion_base_common_update_operations($sandbox, $prefixes, __DIR__);
  return $msg;
}

function trinion_zadachnik_update_8032(&$sandbox) {
  $prefixes = [
    'core.entity_view_display.node.zadacha.default',
  ];
  $msg = trinion_base_common_update_operations($sandbox, $prefixes, __DIR__);
  return $msg;
}

function trinion_zadachnik_update_8033(&$sandbox) {
  $prefixes = [
    'views.view.zadachi_naznachennye_mne',
  ];
  $msg = trinion_base_common_update_operations($sandbox, $prefixes, __DIR__);
  return $msg;
}

function trinion_zadachnik_update_8034(&$sandbox) {
  $prefixes = [
    'user.role.t_zadachnik',
    'system.action.user_remove_role_action.t_zadachnik',
    'system.action.user_add_role_action.t_zadachnik',
    'views.view.zadachi_naznachennye_mne',
  ];
  $msg = trinion_base_common_update_operations($sandbox, $prefixes, __DIR__);
  return $msg;
}

function trinion_zadachnik_update_8035(&$sandbox) {
  $role_object = \Drupal\user\Entity\Role::load('t_zadachnik');
  $role_object->grantPermission('create zadacha content');
  $role_object->grantPermission('edit own zadacha content');
  $role_object->grantPermission('use views bulk edit');
  $role_object->save();
  $msg = 'Role t_zadachnik is created';
  return $msg;
}

function trinion_zadachnik_update_8036(&$sandbox) {
  $prefixes = [
    'views.view.zadachi_naznachennye_mne',
  ];
  $msg = trinion_base_common_update_operations($sandbox, $prefixes, __DIR__);
  return $msg;
}

function trinion_zadachnik_update_8037(&$sandbox) {
  $prefixes = [
    'core.entity_view_display.node.zadacha.default',
    'views.view.zadachi_naznachennye_mne',
  ];
  $msg = trinion_base_common_update_operations($sandbox, $prefixes, __DIR__);
  return $msg;
}

function trinion_zadachnik_update_8038(&$sandbox) {
  $prefixes = [
    'views.view.zadachi_naznachennye_mne',
  ];
  $msg = trinion_base_common_update_operations($sandbox, $prefixes, __DIR__);
  return $msg;
}

function trinion_zadachnik_update_8039(&$sandbox) {
  $prefixes = [
    'views.view.zadachi_naznachennye_mne',
  ];
  $msg = trinion_base_common_update_operations($sandbox, $prefixes, __DIR__);
  return $msg;
}

function trinion_zadachnik_update_8040(&$sandbox) {
  $prefixes = [
    'views.view.zadachi_naznachennye_mne',
  ];
  $msg = trinion_base_common_update_operations($sandbox, $prefixes, __DIR__);
  return $msg;
}

function trinion_zadachnik_update_8041(&$sandbox) {
  $prefixes = [
    'views.view.zadachi_naznachennye_mne',
  ];
  $msg = trinion_base_common_update_operations($sandbox, $prefixes, __DIR__);
  return $msg;
}

function trinion_zadachnik_update_8042(&$sandbox) {
  $config = \Drupal::configFactory()->getEditable('core.entity_form_display.user.user.default');
  if (!$config->isNew()) {
    $config->set('content.field_tz_sotrudnik', [
      'type' => 'boolean_checkbox',
      'weight' => '10',
      'region' => 'content',
      'settings' => ['display_label' => TRUE],
      'third_party_settings' => [],
    ]);
    $config->clear('hidden.field_tz_sotrudnik');
  }

  $prefixes = [
    'field.storage.comment.field_tz_private',
    'field.field.comment.tz_k_zadache.field_tz_private',
    'field.storage.user.field_tz_sotrudnik',
    'field.field.user.user.field_tz_sotrudnik',
    'core.entity_view_display.comment.tz_k_zadache.default',
    'core.entity_form_display.comment.tz_k_zadache.default',
  ];
  $msg = trinion_base_common_update_operations($sandbox, $prefixes, __DIR__);
  return $msg;
}

function trinion_zadachnik_update_8043(&$sandbox) {
  $prefixes = [
    'views.view.zadachi_naznachennye_mne',
  ];
  $msg = trinion_base_common_update_operations($sandbox, $prefixes, __DIR__);
  return $msg;
}
