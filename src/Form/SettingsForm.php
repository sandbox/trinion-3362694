<?php

namespace Drupal\trinion_zadachnik\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

/**
 * Configure TrinionZadachnik settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trinion_zadachnik_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['trinion_zadachnik.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['link'] = [
      '#theme'=> 'item_list',
      '#title' => 'That module use',
      '#html' => TRUE,
      '#items' => [
        ['#markup' => '<a href="/admin/structure/taxonomy/manage/deatelnost_zadacha/overview">' . t('Activity') . '</a>'],
        ['#markup' => '<a href="/admin/structure/taxonomy/manage/prioritet_zadachi/overview">' . t('Priority') . '</a>'],
        ['#markup' => '<a href="/admin/structure/taxonomy/manage/kategoriya_zadachi/overview">' . t('Task category') . '</a>'],
        ['#markup' => '<a href="/admin/structure/taxonomy/manage/tz_proekt/overview">' . t('Projects') . '</a>'],
        ['#markup' => '<a href="/admin/structure/taxonomy/manage/tz_statusy_zadach/overview">' . t('Task statuses') . '</a>'],
      ],
    ];

    $users = $this->config('trinion_zadachnik.settings')->get('default_responsible');
    if ($users)
      $users = User::loadMultiple($users);
    $form['caption'] = [
      '#markup' => '<h3>' . t('Fill this fields') . '</h3>',
    ];
    $form['default_responsible'] = [
      '#type' => 'entity_autocomplete',
      '#title' => 'Ответственные по умолчанию',
      '#description' => 'Будут подставлены в поле Ответственный в задачах, которые создаются на основе электронного письма',
      '#default_value' => $users,
      '#target_type' => 'user',
      '#tags' => TRUE,
    ];
    if (\Drupal::service('module_handler')->moduleExists('trinion_mail'))
      $form['link']['#items'][] = ['#markup' => '<a href="/admin/config/mail/boxes-list">' . t('Mail boxes list') . '</a>'];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $users = [];
    foreach ($form_state->getValue('default_responsible') as $item)
      $users[] = $item['target_id'];
    $this->config('trinion_zadachnik.settings')
      ->set('default_responsible', $users)
      ->save();
    parent::submitForm($form, $form_state);
  }

}
