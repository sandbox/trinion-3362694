<?php

namespace Drupal\trinion_zadachnik\Form;

use Drupal\comment\Entity\Comment;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;


class TaskStatusForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trinion_zadachnik_task_status';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $params = NULL) {
    $options = [];
    foreach (\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('tz_statusy_zadach', 0, 1, FALSE) as $term) {
      $options[$term->tid] = $term->name;
    }
    $node = Node::load($params['nid']);
    $form['status'] = [
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $node->get('field_tz_status_zadachi')->getString(),
      '#ajax' => [
        'callback' => '::ajaxCallback',
        'event' => 'change',
        'wrapper' => 'trinion-zadachnik-task-status',
      ],
    ];

    $form['nid'] = [
      '#type' => 'hidden',
      '#value' => $params['nid']
    ];

    return $form;
  }

  function ajaxCallback(array &$form, FormStateInterface $form_state) {
    $new_status = Term::load($form_state->getValue('status'));
    $node_id = $form_state->getValue('nid');
    $node = Node::load($node_id);
    $node->field_tz_status_zadachi = $new_status;
    $node->save();
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
