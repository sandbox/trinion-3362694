<?php

namespace Drupal\trinion_zadachnik\Form;

use Drupal\Component\Utility\ArgumentsResolver;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 * Fast Task form
 */
class FastTaskForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trinion_zadachnik_fast_task';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['text'] = [
      '#type' => 'textfield',
      '#title' => 'Fast task',
      '#placeholder' => t('Task title'),
      '#rows' => 1,
      '#required' => TRUE,
      '#attributes' => ['data-fast-task-text' => 1],
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Create task',
      '#attributes' => ['class' => ['primary']],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $text = '<p>' . $form_state->getValue('text') . '</p>';
    \Drupal::service('trinion_zadachnik.helper')->createTask($text, NULL, \Drupal::currentUser()->id());
  }
}
