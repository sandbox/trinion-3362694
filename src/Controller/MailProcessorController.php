<?php

namespace Drupal\trinion_zadachnik\Controller;

/**
 * Returns responses for TrinionZadachnik routes.
 */
class MailProcessorController {

  /**
   * Process mail
   * @param $mail_node
   * @return void
   */
  public function processMailMessage($mail_node) {
    $subject = $mail_node->label();
    $files = [];
    foreach ($mail_node->get('field_tm_files') as $file)
      $files[] = $file->getString();
    $data = $this->getMailType($subject);
    $sender_mail = $mail_node->get('field_tm_sender_email')->getString();
    $sender_uid = \Drupal::service('trinion_mail.mails')->getMailAuthorUser($mail_node);
    if ($data['type'] == 'new') {
      $node = \Drupal::service('trinion_zadachnik.helper')->createTask($mail_node->get('field_tm_text')->getValue()[0]['value'], $subject, NULL, $sender_uid, $files);
      \Drupal::service('trinion_zadachnik.helper')->createComment($node->id(), 'Задача создана на основе электронного письма /node/' . $mail_node->id(), $sender_uid, $sender_mail);
    }
    elseif ($data['type'] == 'comment') {
      $query = \Drupal::entityQuery('node')
        ->condition('type', 'zadacha')
        ->condition('title', $data['num']);
      if ($res = $query->accessCheck()->execute())
        \Drupal::service('trinion_zadachnik.helper')->createComment(reset($res), strip_tags($mail_node->get('field_tm_text')->getValue()[0]['value']), $sender_uid, $sender_mail);
    }
  }

  /**
   * Determine message type. New task or task comment
   * @param $subject
   * @return void
   */
  public function getMailType($subject) {
    if (preg_match('/#\s*(\d+)/', $subject, $match)) {
      $data = [
        'type' => 'comment',
        'num' => $match[1],
      ];
    }
    else {
      $data['type'] = 'new';
    }
    return $data;
  }
}
