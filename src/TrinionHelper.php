<?php

namespace Drupal\trinion_zadachnik;

use Drupal\comment\Entity\Comment;
use Drupal\Component\Utility\Unicode;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

class TrinionHelper {
  public function createTask($text, $title = NULL, $resposible = NULL, $uid = NULL, $files = []) {
    if (is_null($uid))
      $uid = \Drupal::currentUser()->id();
    $config = \Drupal::config('trinion_zadachnik.settings');
    if (is_null($title))
      $title = Unicode::truncate(str_replace(["\n", "\r", ], ' ', strip_tags($text)), 100, TRUE);
    $task_data = [
      'type' => 'zadacha',
      'uid' => $uid,
      'status' => 1,
      'field_tz_tema' => $title,
      'field_tz_opisanie' => [
        'format' => 'full_html',
        'value' => $text
      ],
      'field_tz_proekt' => $config->get('project_nerazobrannaya_tid'),
      'field_tz_status_zadachi' => $config->get('status_novaya_tid'),
      'field_tz_prioritet' => $config->get('prioritet_normalniy'),
      'field_tz_deyatelnost' => $config->get('deatelnost_podderzhka'),
    ];
    if ($files)
      $task_data['field_tz_file'] = $files;
    if (is_null($resposible)) {
      $config = \Drupal::config('trinion_zadachnik.settings');
      if ($default_responsible = $config->get('default_responsible'))
        $task_data['field_tz_otvetstvennyy'] = $default_responsible;
    }
    else
      $task_data['field_tz_otvetstvennyy'] = $resposible;
    $node = Node::create($task_data);
    $node->save();
    return $node;
  }

  public function createComment($task_nid, $text, $uid = NULL, $mail = NULL) {
    if (is_null($uid))
      $uid = \Drupal::currentUser()->id();
    $comment_data = [
      'comment_type' => 'tz_k_zadache',
      'entity_type' => 'node',
      'field_name' => 'field_tz_comments',
      'status' => 1,
      'uid' => $uid,
      'subject' => 'Комментарий',
      'entity_id' => $task_nid,
      'field_tz_kommentariy' => [['value' => $text]],
    ];
    if (!is_null($mail))
      $comment_data['mail'] = $mail;
    $comment = Comment::create($comment_data);
    $comment->save();
  }

  public function taskCommentsAndChangesList($comments, $node) {
    if (is_numeric($node))
      $node = Node::load($node);
    function relative($timestamp) {
      return str_replace([date('d.m.Y', strtotime('-1 day')), date('d.m.Y')], [t('yesterday'), t('today')], date('d.m.Y', $timestamp));
    }
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'change_log')
      ->condition('field_tcl_object', $node->id());
    $res = $query->accessCheck()->execute();
    $lines = [];
    foreach ($res as $cid) {
      $change = Node::load($cid);
      $change->date = relative($change->get('created')->getString());
      $change->time = date('H:i', $change->get('created')->getString());
      $lines[$change->get('created')->getString()] = $change;
    }
    $is_sotrudnik = $this->isUserSotrudnik();
    foreach ($comments as $key => $comment) {
      if (is_numeric($key) && isset($comment['#comment'])) {
        if (!$is_sotrudnik && $comment['#comment']->get('field_tz_private')->getString()) {
          continue;
        }
        $created = $comment['#comment']->get('created')->getString();
        while (isset($lines[$created]))
          $created++;
        $comment_text = $comment['#comment']->get('field_tz_kommentariy')->getValue()[0]['value'];
        if (preg_match('/#(\d+)\[(\d+)\]/', $comment_text, $match)) {
          if ($reply = Comment::load($match[2])) {
            $reply_text = $reply->get('field_tz_kommentariy')->getValue()[0]['value'];
            $comment['#comment']->reply_to = [
              'text' => Unicode::truncate($reply_text, 100, 1, 1),
              'author' => $reply->get('uid'),
              'date' => relative($reply->get('created')->getString()),
              'time' => date('H:i',$reply->get('created')->getString()),
              'tag' => $match[0],
              'num' => $match[1],
              'cid' => $match[2],
            ];
          }
        }
        $comment['#comment']->date = relative($comment['#comment']->get('created')->getString());
        $comment['#comment']->time = date('H:i', $comment['#comment']->get('created')->getString());
        $lines[$created] = $comment['#comment'];
      }
    }
    ksort($lines);
    return array_values($lines);
  }

  public function getZadachaDeyatelnostName($task) {
    return ($value = $task->get('field_tz_deyatelnost')->first()) ? $value->entity->label() : '';
  }

  public function getZadachaProjectName($task) {
    return ($value = $task->get('field_tz_proekt')->first()) ? $value->entity->label() : '';
  }

  public function getZadachaStatusName($task) {
    return ($value = $task->get('field_tz_status_zadachi')->first()) ? $value->entity->label() : '';
  }

  public function getZadachaProirityName($task) {
    return ($value = $task->get('field_tz_prioritet')->first()) ? $value->entity->label() : '';
  }

  public function getZadachaReponsibleName($task) {
    $res = [];
    foreach ($task->get('field_tz_otvetstvennyy') as $item)
      $res[] = \Drupal::service('trinion_main.helper')->getNameOrLogin($item->entity);
    foreach ($task->get('field_tz_role') as $item)
      $res[] = $item->entity->label();
    return implode(', ', $res);
  }

  public function isUserSotrudnik() {
    $user = User::load(\Drupal::currentUser()->id());
    return $user->get('field_tz_sotrudnik')->getString();
  }
}
